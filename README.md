#Plivy Job Portal

A job portal for consultants to filter their job applicants.


#Prerequisites

Operating system
>mac,

>linux

Database
>sqlite3

Languages Used
>Python,

>Angularjs

ORM Used
>SQLAlchemy

Python packages used

>flask,

>flask-admin,

>sqlalchemy,

>enum,

>python-dateutil

Browser Prerequisite
>Chrome


#Installation 
Create a virtualenv

>virtualenv venv

Activate virtualenv

>source venv/bin/activate

Install python packages using setup.py, run the following command

>python setup.py

Once this is done, using models build the db and place the file in crud folder using the following commands

>python models/models.py

Move the file from db model from models to crud

>mv plivy.db ../crud/

After this, we are ready to use the tool run the following command

>python app.py

This will run the server on port number 5000,open browser and search for http://localhost:5000 


#Endpoints and descriptions

FUNCTIONS

Filename: app.py

insertnewstatus()
    This endpoint is used to update data about a candidate.

newcandidates()
    This endpoint is used to insert new candidates for a job.

newjobs()
    This endpoint is used to insert new jobs.

render_dialer()
    This endpoint is used to render search template.

returncandidates()
    This endpoint is used to return list of candidates.

returncandidatestatus()
    This endpoint is used to return candidatestatus for a particular job.

returnjobs()
    This endpoint is used to return list of jobs.

Filename: insert.py

insertcandidates(data)
    Function to add new applicants for the job.

insertjobs(data)
    Function to insert new jobs.

insertstatus(data)
    Function to insert new updates for a job candidate.

Filename:read.py

returncandidates(receivedjobid)
    Returns list of applicants based on the job id clicked.

returncandidatestatus(receivedcandidateid)
    Returns list of updates about a candidate.

returnjobs(wordtyped)
    Returns jobs based on the typed title name

#Navigating through the application

url:'/'-This is the search page you can add or view list of jobs,applicants and their status
url:'/admin'-This page will help you edit,modify,delete records from the database. You cannot add records because of the flask-admin limitations.

#scope of improvement
-Links for job titles
-Notifications to acknowledge user for certain actions
-login functionality
-UI
-better ux
-filling the first record of job portal
-Title of the job for which candidates are being shown 
-Name of the candidate for which status is being show for


#contact details
For any further queries you can mail me on kunchamharsha@gmail.com,
you can also raise an issue and I will get back to you as soon as possible