import sys
from sqlalchemy import create_engine, and_
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.sqlite import TEXT
from sqlalchemy.sql.expression import delete
from sqlalchemy.orm.exc import NoResultFound
from datetime import datetime
import json
import ast
import requests
from dateutil import parser
from flask import jsonify
import datetime

sys.path.insert(0, '../models/')

from models import User,Candidates,Candidatestatus,Jobs

#initialising Sqlalchemy Engine

engine = create_engine('sqlite:///crud/plivy.db')
Base = declarative_base() 
DBSession = sessionmaker(bind=engine)

def insertjobs(data):
    """
    Function to insert new jobs.
    """
    session=DBSession()
    jobs = Jobs(jobtitle=data['jobtitle'], company=data['company'],dateposted=datetime.datetime.strptime(data['dateposted'], '%Y-%m-%d'),status=data['status'])
    session.add(jobs)
    session.commit()
    return 'Successful'

def insertcandidates(data):
    """
    Function to add new applicants for the job.
    """
    session=DBSession()
    candidates=Candidates(jobid=data['jobid'],ctc=data['ctc'],experience=data['experience'],currentcompany=data['currentcompany'],name=data['name'])
    session.add(candidates)
    session.commit()
    return 'Successful'

def insertstatus(data):
    """
    Function to insert new updates for a job candidate.
    """
    session=DBSession()
    candidates=Candidatestatus(candidateid=data['candidateid'],interview=data['interview'],interviewer=data['interviewer'],interviewdate=datetime.datetime.strptime(data['interviewdate'],'%Y-%m-%d'),result=data['result'])    
    session.add(candidates)
    session.commit()
    return 'Successful'
