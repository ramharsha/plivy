import sys
from sqlalchemy import create_engine, and_
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.sqlite import TEXT
from sqlalchemy.sql.expression import delete
from sqlalchemy.orm.exc import NoResultFound
from datetime import datetime
import json
import ast
import requests
from flask import jsonify

sys.path.insert(0, '../models/')

from models import User,Candidates,Candidatestatus,Jobs

#initialising Sqlalchemy Engine

engine = create_engine('sqlite:///crud/plivy.db')
Base = declarative_base() 
DBSession = sessionmaker(bind=engine)

def returnjobs(wordtyped,status):
    """
    Returns jobs based on the typed title name
    """        
    session=DBSession()
    if len(status)!=2:
        jobs=session.query(Jobs).filter(and_(Jobs.jobtitle.like('%'+wordtyped+'%'),Jobs.status==status[0]))
    else:
        jobs=session.query(Jobs).filter(Jobs.jobtitle.like('%'+wordtyped+'%'))        
    listofjobs=[]
    for job in jobs:
        jobdata={}
        jobdata['id']=job.jobid
        jobdata['jobtitle']=job.jobtitle
        jobdata['company']=job.company
        jobdata['dateposted']=job.dateposted
        jobdata['status']=job.status
        listofjobs.append(jobdata)
    return jsonify(listofjobs)

def returncandidates(receivedjobid):
    """
    Returns list of applicants based on the job id clicked.
    """
    session=DBSession()
    jobs=session.query(Candidates).filter_by(jobid=receivedjobid)
    listofcandidates=[]
    for job in jobs:
        print job
        jobholderdata={}
        jobholderdata['id']=job.applyid
        jobholderdata['ctc']=job.ctc
        jobholderdata['experience']=job.experience
        jobholderdata['company']=job.currentcompany
        jobholderdata['name']=job.name
        listofcandidates.append(jobholderdata)
    return json.dumps(listofcandidates)

def returncandidatestatus(receivedcandidateid):
    """
    Returns list of updates about a candidate.
    """
    print receivedcandidateid
    session=DBSession()
    jobs=session.query(Candidatestatus).filter_by(candidateid=receivedcandidateid)
    listofjobs=[]
    for job in jobs:
        jobdata={}
        jobdata['statusid']=job.statusid
        jobdata['interview']=job.interview
        jobdata['interviewer']=job.interviewer
        jobdata['date']=job.interviewdate
        jobdata['result']=job.result
        listofjobs.append(jobdata)
    return jsonify(listofjobs)