from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Boolean, VARCHAR, Integer, String, DateTime, ForeignKey, UniqueConstraint, PrimaryKeyConstraint,Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.sqlite import TEXT
import uuid

engine = create_engine('sqlite:///plivy.db', encoding="utf-8")

Base = declarative_base() 
DBSession = sessionmaker(bind=engine)


def generate_uuid():
    return str(uuid.uuid4())

session = DBSession()
class User(Base):
    __tablename__ = 'user'
    name = Column(VARCHAR(256))
    email = Column(VARCHAR(256))
    userid = Column(VARCHAR(36), primary_key=True)
    password = Column(VARCHAR(256))
    verified = Column(Boolean, default=False)

    def is_active(self):
        """True, as all users are active."""
        return True

    def get_id(self):
        """Return the email address to satisfy Flask-Login's requirements."""
        return self.userid

    def is_authenticated(self):
        """Return True if the user is authenticated."""
        return True

    def is_anonymous(self):
        """False, as anonymous users aren't supported."""
        return False

class Jobs(Base):
    __tablename__='jobs'
    jobid=Column(VARCHAR(36),primary_key=True,default=generate_uuid)
    jobtitle=Column(VARCHAR(256))
    company=Column(VARCHAR(128))
    dateposted=Column(DateTime)
    status=Column(VARCHAR(256))


class Candidates(Base):
    __tablename__='candidatedetails'
    applyid=Column(VARCHAR(36),primary_key=True,default=generate_uuid)
    jobid=Column(VARCHAR(256),ForeignKey('jobs.jobid'),nullable=False)
    ctc=Column(Float)
    experience=Column(Float)
    currentcompany=Column(VARCHAR(256))
    name=Column(VARCHAR(256))


class Candidatestatus(Base):
    __tablename__='candidatestatus'
    statusid=Column(VARCHAR(36),primary_key=True,default=generate_uuid)
    candidateid=Column(VARCHAR(256),ForeignKey('candidatedetails.applyid'),nullable=False)
    interview=Column(VARCHAR(256))
    interviewer=Column(VARCHAR(256))
    interviewdate=Column(DateTime)
    result=Column(VARCHAR(256))
    

if __name__ == "__main__":
    Base.metadata.create_all(bind=engine)