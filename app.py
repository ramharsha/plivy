from flask import render_template, app,Flask,request
import sys
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from sqlalchemy import create_engine, and_
from sqlalchemy.orm import sessionmaker,scoped_session
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.sqlite import TEXT
from sqlalchemy.sql.expression import delete
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.pool import SingletonThreadPool
import json

app=Flask(__name__,static_url_path='')
admin = Admin(app)


sys.path.insert(0, './crud/')
sys.path.insert(0, './models/')

import read,insert

from models import User, Jobs, Candidates, Candidatestatus

class MyModel(ModelView):
      column_display_pk = True
      list_columns=['jobid','jobtitle','company','dateposted','status']

engine = create_engine('sqlite:///./crud/plivy.db',poolclass=SingletonThreadPool)
Base = declarative_base() 
session= scoped_session(sessionmaker(bind=engine))

#admin.add_view(ModelView(User, session))
admin.add_view(MyModel(Jobs, session))
admin.add_view(ModelView(Candidates, session))
admin.add_view(ModelView(Candidatestatus, session))



""""@app.route('/')
def render_loginpage():
    
    #This endpoint is used to render loginpage of the application.
    
    return render_template('loginpage.html')"""


@app.route('/')
def render_dialer():
    """
    This endpoint is used to render search template.
    """
    return render_template('searchpage.html')


@app.route('/api/returnjobs',methods=['GET'])
def returnjobs():
    """
    This endpoint is used to return list of jobs.
    """
    keyword=request.args.get('searchterm')
    status=request.args.getlist('status')
    print status
    return read.returnjobs(keyword,status)


@app.route('/api/returncandidates',methods=['GET'])
def returncandidates():
    """
    This endpoint is used to return list of candidates.
    """
    keyword=request.args.get('id')
    return read.returncandidates(keyword)



@app.route('/api/insertnewcandidates',methods=['POST'])
def newcandidates():
    """
    This endpoint is used to insert new candidates for a job.
    """
    data=json.loads(request.data)
    return insert.insertcandidates(data)


@app.route('/api/insertnewjobs',methods=['POST'])
def newjobs():
    """
    This endpoint is used to insert new jobs.
    """
    data=json.loads(request.data)
    print data
    return insert.insertjobs(data)


@app.route('/api/insertnewstatus',methods=['POST'])
def insertnewstatus():
    """
    This endpoint is used to update data about a candidate.
    """
    data=json.loads(request.data)
    return insert.insertstatus(data)




@app.route('/api/returnstatus',methods=['GET'])
def returncandidatestatus():
    """
    This endpoint is used to return candidatestatus for a particular job.
    """
    candidateid=request.args.get('candidateid')
    return read.returncandidatestatus(candidateid)




app.config['static_url_path'] ='/static'
app.config['SECRET_KEY'] = 'AZXSDM11233108123A'

if __name__=='__main__':
    app.run(host='127.0.0.1',port=5000,debug=True)