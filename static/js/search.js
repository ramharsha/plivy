app.controller('search',function($http,$scope){
    $scope.jobtitle=''
    
    $scope.updatejobs=function(){
        console.log('hello')
        $scope.status=[]
        if(document.getElementById('opencheck').checked==true){
            $scope.status.push('open')            
        }
        if(document.getElementById('closecheck').checked==true){
            $scope.status.push('close')
        }
        if($scope.status.length==0){
            $scope.status.push('open')
            $scope.status.push('close')
        }
      
        return $http({url:'/api/returnjobs',method:'GET',params:{'searchterm':$scope.jobtitle,'status':$scope.status}}).then(function(response,status){
            $scope.data.jobs=response.data
        });
    }
    
    //get candidates of the candidates who applied for the job

    $scope.changedata=function(id,jobtitle){
        console.log(id)
        return $http.get('/api/returncandidates?id='+id).then(function(response,status){
            $scope.selectedjob=response.data;
            $scope.selectedjobid=id        
            $scope.selectedjobtitle=jobtitle    
        });
        console.log($scope.selectedjob);
    }
    
    //Interview status of the candidate 
    $scope.showstatus=function(candidateid,jobid,candidatename){
        /*console.log(candidateid)
        console.log(jobid)
        for(i=0;i<$scope.data.jobs.length;i++){
            if($scope.data.jobs[i].id==jobid){
                for(j=0;j<$scope.data.jobs[i]['candidates'].length;j++){
                    if($scope.data.jobs[i].candidates[j].details.id==candidateid){                
                        $scope.candidatestatus=$scope.data.jobs[i].candidates[j].status;                
                    }
                }
            }
        }*/

        return $http.get('/api/returnstatus',{params:{'candidateid':candidateid,'jobid':jobid}}).then(function(response,status){
            $scope.candidatestatus=response.data
            $scope.selectedcandidateid=candidateid
            $scope.selectedcandidatename=candidatename
        })
    }
    
    
    $scope.data={
        jobs:[]
    }
    $scope.selectedjob=[]
    $scope.candidatestatus=[]

    $scope.addnewjobs=function(){
        $scope.newjobdata={
            'jobtitle':document.getElementById('job_title').value,
            'company':document.getElementById('offering_company').value,
            'dateposted':document.getElementById('jobdateposted').value,
            'status':document.getElementById('status_of_job').value
        }
        $http({url:'/api/insertnewjobs',data:$scope.newjobdata,method:'POST'}).then(function(response,status){
            $scope.updatejobs()
        })
    }

    $scope.addnewcandidates=function(){
        $scope.newcandidatedata={
            'name':document.getElementById('name').value,
            'currentcompany':document.getElementById('cuc').value,
            'ctc':document.getElementById('ctc').value,
            'experience':document.getElementById('experience').value,
            'jobid':$scope.selectedjobid
        }
        $http({url:'/api/insertnewcandidates',data:$scope.newcandidatedata,method:'POST'}).then(function(response,status){
            $scope.updatejobs()
        })
    }


    $scope.addnewcandidatestatus=function(){
        console.log($scope.selectedcandidateid)
        $scope.addnewcandidateupdate={
            'interview':document.getElementById('interview').value,
            'interviewer':document.getElementById('interviewer').value,
            'interviewdate':document.getElementById('date_of_interview').value,
            'result':document.getElementById('result').value,
            'candidateid':$scope.selectedcandidateid
        }
        $http({url:'/api/insertnewstatus',data:$scope.addnewcandidateupdate,method:'POST'}).then(function(response,status){
            $scope.showstatus($scope.selectedcandidateid,$scope.selectedjobid)
        })
    }
})